import React, { useState } from 'react';

import './style.css';

const AddUser = (props) => {
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [age, setAge] = useState('');

    const handleInputChange = (e) => {
        const value = e.currentTarget.value;

        switch (e.currentTarget.name) {
            case 'name':
                setName(value);
                break;
            case 'surName':
                setSurname(value);
                break;
            case 'age':
                setAge(value);
            default:
                return;
        }
    };

    const handleSave = () => {
        //ToDo handle inputs validation

        props.saveUser({name, surname, age});
        setName('');
        setSurname('');
        setAge('');
    };

    return (
        <div className="saveUserContainer">
            <input
                type="text"
                name="name"
                placeholder="Enter Name"
                value={name}
                onChange={handleInputChange}
            />
            <input
                type="text"
                name="surName"
                placeholder="Enter SurName"
                value={surname}
                onChange={handleInputChange}
            />
            <input
                type="date"
                name="age"
                placeholder="Enter Name"
                value={age}
                onChange={handleInputChange}
            />
            <button onClick={handleSave}>
                Add User
            </button>
        </div>
    )
};

export default AddUser;

// ToDo implement proptype validation
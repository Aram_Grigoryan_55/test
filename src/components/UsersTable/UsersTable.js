import React, { useState, useRef, useCallback } from 'react';
import clone from 'lodash/clone';

import Paginate from '../Paginate';

import './style.css';

const UsersTable = (props) => {
    const [deletableItems, setDeletableItems] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [perPage, setPerPage] = useState(3);

    let users = clone(props.users);

    // setup ref for contentEditable feature
    // for (let i = 0; i < users.length; i++) {
    //     users[i].nameRef = React.createRef();
    //     users[i].surnameRef = React.createRef();
    // }

    // shorten users for pagination
    const lastIndex = currentPage * perPage;
    const firstIndex = lastIndex - perPage;
    users = users.slice(firstIndex, lastIndex);

    const handlePageChange = (id) => {
        setCurrentPage(id);
    };

    const handleCheckBoxChange = (e) => {
        const id = e.currentTarget.dataset.id;
        let items = clone(deletableItems);

        if (items.includes(id)) {
            const index = items.indexOf(id);
            if (index > -1) {
                items.splice(index, 1);
                setDeletableItems(items);
            }
        } else {
            setDeletableItems(items.concat([id]));
        }
    };

    const handleContentChange = (e) => {
        console.log(e.currentTarget, 888888)
    };

    const handleDelete = () => {
        // TODO implement deletion based on id, not index, for pagination rules

        props.handleUserBulkDelete(deletableItems);
        setDeletableItems([]);
    };

    const handleDoubleClick = useCallback((ref) => {
        ref.current.contentEditable = true;
        ref.current.focus();
    });

    const handleBlur = useCallback((ref) => {
        ref.current.contentEditable = false;
    });

    const calculateAge = (value) => {
        let today = new Date();
        let birthDate = new Date(value);
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age = age - 1;
        }
        return age;
    };

    const renderUsers = users.map((user, index) => {
        return (
            <tr key={`${user.name}${user.surname}${user.age}`}>
                <td
                    ref={user.nameRef}
                    onChange={handleContentChange}
                    onDoubleClick={() => handleDoubleClick(user.nameRef)}
                    onBlur={() => handleBlur(user.nameRef)}
                >{user.name}</td>
                <td
                    ref={user.surnameRef}
                    onChange={handleContentChange}
                    onDoubleClick={() => handleDoubleClick(user.surnameRef)}
                    onBlur={() => handleBlur(user.surnameRef)}
                >{user.surname}</td>
                <td>{calculateAge(user.age)}</td>
                {props.batchDelete && (
                    <td>
                        <input
                            type="checkbox"
                            data-id={index}
                            checked={deletableItems.includes(index.toString())}
                            onChange={handleCheckBoxChange}
                        />
                    </td>
                )}
            </tr>
        );
    });

    return (
        <div className="tableContainer">
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>age</th>
                        {props.batchDelete && (
                            <th>delete</th>
                        )}
                    </tr>
                </thead>
                <tbody>{renderUsers}</tbody>
            </table>
            {deletableItems.length ? (
                <button onClick={handleDelete}>Delete</button>
            ) : null}
            <Paginate
                currentPage={currentPage}
                itemLength={props.users.length}
                perPage={perPage}
                handlePageChange={handlePageChange}
            />
        </div>
    )
};

export default UsersTable;

// ToDo implement proptype validation
//armen.mshetsyan@fluxtech.me
import React from 'react';

import './style.css';

const Paginate = (props) => {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(props.itemLength / props.perPage); i++) {
        pageNumbers.push(i);
    }

    const handleClick = (e) => {
        props.handlePageChange(e.currentTarget.id);
    };

    const renderPageNumbers = pageNumbers.map(number => {
        return (
            <span key={number} id={number} onClick={handleClick}>
                {number}
            </span>
        );
    });

    return (
        <div className="pagination">
            {renderPageNumbers}
        </div>
    )
};

export default Paginate;

// ToDo implement proptype validation
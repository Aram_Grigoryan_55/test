import React, { Component } from 'react';
import clone from 'lodash/clone';

import UsersTable from '../components/UsersTable';
import AddUser from '../components/AddUser';

import './style.css';

class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            user: [],
            batchDelete: true
        }
    }

    componentDidMount(){
        window.addEventListener('storage', this.setUsersFromStorage);
        this.setUsersFromStorage();
        // ToDo fetch users from API and set the state
        // ToDo set user in local storage
    }

    componentDidUpdate(){
        //ToDo handle save user to database if necessary
    }

    componentWillUnmount(){
        window.removeEventListener('storage', this.setUsersFromStorage);
    }

    setUsersFromStorage = () => {
        let user = localStorage.getItem('user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user });
        }
    };

    handleUserSave = (data) => {
        let user = clone(this.state.user);
        user.push(data);
        // for(let i = 0; i < user.length; i++) {
        //     console.log(user[i].nameRef, 55555);
        //     delete user[i].nameRef;
        //     console.log(user[i].nameRef, 6666);
        //     delete user[i].surnameRef;
        // }
        // console.log(user, 888888)
        this.setState({ user }, () => {
            localStorage.setItem('user', JSON.stringify(user));
        });
    };

    handleBatchDeleteMode = () => {
        this.setState({batchDelete: !this.state.batchDelete})
    };

    handleUserBulkDelete = (indexes) => {
        const { user } = {...this.state};
        for (let i = 0; i < indexes.length; i++) {
            user.splice(+indexes[i], 1);
        }
        this.setState({ user });
    };

    render (){
        return (
            <>
                <label className="switch">
                    <input
                        type="checkbox"
                        checked={this.state.batchDelete}
                        onChange={this.handleBatchDeleteMode}
                    />
                    <span className="slider round"></span>
                </label>
                <UsersTable
                    users={this.state.user}
                    batchDelete={this.state.batchDelete}
                    handleUserBulkDelete={this.handleUserBulkDelete}
                />
                <AddUser saveUser={this.handleUserSave} />
            </>
        )
    }
}

export default Home;

// ToDo implement proptype validation